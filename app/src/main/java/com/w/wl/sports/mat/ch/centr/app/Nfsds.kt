package com.w.wl.sports.mat.ch.centr.app

import android.app.Application
import com.backendless.Backendless
import com.yandex.metrica.YandexMetrica
import com.yandex.metrica.YandexMetricaConfig

class Nfsds: Application(){

    override fun onCreate() {
        super.onCreate()
        val config = YandexMetricaConfig.newConfigBuilder("502cdb74-0b34-4fe1-9150-a12ab03ea09b").build()

        YandexMetrica.activate(applicationContext, config)
        YandexMetrica.enableActivityAutoTracking(this)

        Backendless.setUrl("https://api.backendless.com")
        Backendless.initApp(applicationContext, "471636C6-C0E7-F51E-FF8E-5AFE9CC0E700", "6EACB039-4A0F-48EE-9867-82291A6F6230")
    }


}