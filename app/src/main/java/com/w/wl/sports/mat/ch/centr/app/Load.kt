package com.w.wl.sports.mat.ch.centr.app

import android.animation.Animator
import android.animation.Animator.AnimatorListener
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.edit
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.backendless.Backendless
import com.w.wl.sports.mat.ch.centr.app.R
import com.w.wl.sports.mat.ch.centr.app.databinding.LoadBinding
import com.w.wl.sports.mat.ch.centr.app.Libr123Msj.fdsfds
import com.w.wl.sports.mat.ch.centr.app.Libr123Msj.iyuiuty
import com.w.wl.sports.mat.ch.centr.app.Libr123Msj.prewvsd
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.net.HttpURLConnection
import java.net.URL


class Load: Fragment() {

    var finish = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val b = LoadBinding.inflate(inflater, container, false)

        val sp = requireActivity().getSharedPreferences(
            Libr123Msj.SPPP,
            Context.MODE_PRIVATE
        )
        if (sp.getString(Libr123Msj.link, "") == "") {
            try {
                lifecycleScope.launch(Dispatchers.IO) {
                    val x = Backendless.Data.of(fdsfds)
                        .findById(iyuiuty)[fdsfds]
                    if (x != null){
                        val finalUrl = getFinalURL(x.toString())
                        if (finalUrl?.contains(prewvsd) == true) {
                            sp.edit {
                                putString(Libr123Msj.link, "null")
                                commit()
                            }
                        } else {
                            sp.edit {
                                putString(Libr123Msj.link, x.toString())
                                commit()
                            }
                        }
                    }
                    finish = true
                }
            } catch (e: Exception) {
                finish = true
            }
        }else{
            findNavController().navigate(R.id.mmain)
        }


        b.lav.playAnimation()
        b.lav.addAnimatorListener(object : AnimatorListener {
            override fun onAnimationStart(animation: Animator) {
            }

            override fun onAnimationEnd(animation: Animator) {
            }

            override fun onAnimationCancel(animation: Animator) {
            }

            override fun onAnimationRepeat(animation: Animator) {
                if (finish)
                    findNavController().navigate(R.id.mmain)
            }
        })



        return b.root
    }

    private fun getFinalURL(url: String?): String? {
        val con = URL(url).openConnection() as HttpURLConnection
        con.instanceFollowRedirects = false
        con.connect()
        con.inputStream
        if (con.responseCode == HttpURLConnection.HTTP_MOVED_PERM || con.responseCode == HttpURLConnection.HTTP_MOVED_TEMP) {
            val redirectUrl = con.getHeaderField("Location")
            return getFinalURL(redirectUrl)
        }
        return url
    }
}