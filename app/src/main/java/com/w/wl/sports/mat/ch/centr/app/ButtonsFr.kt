package com.w.wl.sports.mat.ch.centr.app

import android.content.Context
import android.os.Bundle
import android.text.format.DateFormat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.w.wl.sports.mat.ch.centr.app.databinding.FrmBinding
import com.w.wl.sports.mat.ch.centr.app.netete.Clss
import com.w.wl.sports.mat.ch.centr.app.netete.ffdsfds.bdkfnglkfn
import com.w.wl.sports.mat.ch.centr.app.netete.ffdsfds.bdkfnglkfnImpl
import com.w.wl.sports.mat.ch.centr.app.rvv.wwrytuweuytw

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.Calendar

class ButtonsFr: Fragment() {

    lateinit var bind: FrmBinding

    val list: MutableLiveData<List<Clss>> = MutableLiveData(listOf())
    val a = wwrytuweuytw()

    lateinit var rep: bdkfnglkfn

    val timezone = Calendar.getInstance().timeZone.toZoneId().toString()
    var tod = ""
    var tom = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        bind = FrmBinding.inflate(inflater, container, false)

        bind.materialButton2.setOnClickListener {
            findNavController().popBackStack()

        }
        bind.lav.visibility = View.VISIBLE


        val sp = requireActivity().getSharedPreferences(
            Libr123Msj.SPPP,
            Context.MODE_PRIVATE
        )

        rep = when(sp.getInt(Libr123Msj.sport, 0)) {
            0 -> bdkfnglkfnImpl(requireContext(), "https://v3.football.api-sports.io")
            1 -> bdkfnglkfnImpl(requireContext(), "https://v1.hockey.api-sports.io/")
            else -> bdkfnglkfnImpl(requireContext(), "https://v1.volleyball.api-sports.io")
        }

        with(bind.russkayaRevolusion){
            adapter = a
            layoutManager = LinearLayoutManager(requireContext())
        }

        val date = Calendar.getInstance()
        val date1 = DateFormat.format("yyyy-MM-dd", date.time).toString()
        date.set(Calendar.DAY_OF_YEAR, date.get(Calendar.DAY_OF_YEAR) + 1)
        val date2 = DateFormat.format("yyyy-MM-dd", date.time).toString()

        funciaPrivet(date1, date2)

        list.observeForever {
            a.submitList(it)
        }

        return bind.root

    }
    private fun funciaPrivet(date1: String, date2: String) {
        bind.russkayaRevolusion.visibility = View.INVISIBLE

        val sp = requireActivity().getSharedPreferences(
            Libr123Msj.SPPP,
            Context.MODE_PRIVATE
        )


        lifecycleScope.launch(Dispatchers.IO) {
            requireActivity().runOnUiThread {
            }
            val l = mutableListOf<Clss>()
            when(sp.getInt(Libr123Msj.sport, 0)) {
                0 -> {
                    rep.gggewwtwt(date1, 1, "2024", "fsdds", "dsdsadaf", timezone).collect { response ->
                        response.forEach { it.fdfds = "Сегодня" }
                        l.addAll(response)

                        Log.d("fdsfds", response.toString())
                        list.postValue(l)
                    }
                    rep.gggewwtwt(date2, 1, "2024","football", "day",  timezone).collect { response ->
                        response.forEach { it.fdfds = "Завтра" }
                        l.addAll(response)
                        list.postValue(l)
                    }
                }
                1 -> {
                    listOf(57, 125, 58).forEach { leag ->
                        rep.uutiyity(date1, leag, "2024", "anekdot", "12:00", timezone).collect { response ->
                            response.forEach { it.fdfds = "Сегодня" }
                            l.addAll(response)
                            list.postValue(l)
                        }
                        rep.uutiyity(date2, leag, "2024", "anekdot", "12:00",timezone).collect { response ->
                            response.forEach { it.fdfds = "Завтра" }
                            l.addAll(response)
                            list.postValue(l)
                        }
                    }
                    bdkfnglkfnImpl(requireContext(), "https://v1.hockey.api-sports.io/")
                }
                else -> {
                    rep.iuyuytouot(date1, 1, "2024", timezone, "https://v1.hockey.api-sports.io/", "left").collect { response ->
                        response.forEach { it.fdfds = "Сегодня" }
                        l.addAll(response)
                        list.postValue(l)
                    }
                    rep.iuyuytouot(date2, 1, "2024", timezone,"https://v1.hockey.api-sports.io/", "left").collect { response ->
                        response.forEach { it.fdfds = "Завтра" }
                        l.addAll(response)
                        list.postValue(l)
                    }
                }
            }

            requireActivity().runOnUiThread {
                if (list.value?.isEmpty() == true)
                    bind.nothing.visibility = View.VISIBLE
                else
                    bind.russkayaRevolusion.visibility = View.VISIBLE
                bind.lav.visibility = View.GONE

            }
        }
    }
}