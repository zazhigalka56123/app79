package com.w.wl.sports.mat.ch.centr.app

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.w.wl.sports.mat.ch.centr.app.R

class MainAct : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.a2)

        val sp = getSharedPreferences(
            Libr123Msj.SPPP,
            Context.MODE_PRIVATE
        )
        Log.d("fsdfdsf",sp.getBoolean(Libr123Msj.pervyRaz, true).toString() )
        if (sp.getBoolean(Libr123Msj.pervyRaz, true)) {
            startActivity(Intent(this, ActFIrst::class.java))
            finish()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 123) {
            finishAffinity()
        }
    }

}