package com.w.wl.sports.mat.ch.centr.app

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

class ActFIrst : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.a1)

        val sp = getSharedPreferences(
            Libr123Msj.SPPP,
            Context.MODE_PRIVATE
        )
        if (sp.getBoolean(Libr123Msj.pervyRaz, true) == false) {
            startActivity(Intent(this, MainAct::class.java))
            finish()
        }
    }
}