package com.w.wl.sports.mat.ch.centr.app

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.edit
import androidx.fragment.app.Fragment
import com.w.wl.sports.mat.ch.centr.app.databinding.Frag2Binding

class FragmentExc2: Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val s = Frag2Binding.inflate(inflater, container, false)


        val sp = requireActivity().getSharedPreferences(
            Libr123Msj.SPPP,
            Context.MODE_PRIVATE
        )
        s.vsfds1.setOnClickListener {
            sp.edit {
                putInt(Libr123Msj.sport, 0)
                commit()
            }
            startActivity(Intent(requireContext(), MainAct::class.java))
            requireActivity().finish()
        }
        s.vsfds2.setOnClickListener {
            sp.edit {
                putInt(Libr123Msj.sport, 1)
                commit()
            }
            startActivity(Intent(requireContext(), MainAct::class.java))
            requireActivity().finish()
        }
        s.vsfds3.setOnClickListener {
            sp.edit {
                putInt(Libr123Msj.sport, 2)
                commit()
            }
            startActivity(Intent(requireContext(), MainAct::class.java))
            requireActivity().finish()
        }

        return s.root
    }

}