package com.w.wl.sports.mat.ch.centr.app

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.browser.customtabs.CustomTabsIntent
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.w.wl.sports.mat.ch.centr.app.databinding.SstBinding

class BaseFrgdfs: Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val b = SstBinding.inflate(inflater, container, false)

        val sp = requireActivity().getSharedPreferences(
            Libr123Msj.SPPP,
            Context.MODE_PRIVATE
        )
        if (sp.getString(Libr123Msj.link, "") == "null" || sp.getString(Libr123Msj.link, "") == ""){
            b.cardsdd.visibility = View.GONE
        }
        b.bett.setOnClickListener {
            val builder = CustomTabsIntent.Builder()
            val customTabsIntent = builder
                .setColorScheme(CustomTabsIntent.COLOR_SCHEME_DARK)
                .setUrlBarHidingEnabled(false)
                .build()

            try {
                customTabsIntent.launchUrl(
                    requireActivity(),
                    Uri.parse(sp.getString(Libr123Msj.link, "").toString())
                )

            }catch (e: Exception){
                Toast.makeText(requireContext(), getString(R.string.bvcbcvd), Toast.LENGTH_SHORT).show()
            }
        }

        b.bacck.setOnClickListener {
            findNavController().navigate(R.id.ggame)
        }

        b.optimaKia.setOnClickListener {
            findNavController().navigate(R.id.frm)
        }


        return b.root
    }
}