package com.w.wl.sports.mat.ch.centr.app

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.edit
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.w.wl.sports.mat.ch.centr.app.R
import com.w.wl.sports.mat.ch.centr.app.databinding.Frag2Binding

class Ggame: Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val q = Frag2Binding.inflate(inflater, container, false)

        val sp = requireActivity().getSharedPreferences(
            Libr123Msj.SPPP,
            Context.MODE_PRIVATE
        )
        q.vsfds1.setOnClickListener {
            sp.edit {
                putInt(Libr123Msj.sport, 0)
                commit()
            }
            findNavController().navigate(R.id.mmain)
        }
        q.vsfds2.setOnClickListener {
            sp.edit {
                putInt(Libr123Msj.sport, 1)
                commit()
            }
            findNavController().navigate(R.id.mmain)
        }
        q.vsfds3.setOnClickListener {
            sp.edit {
                putInt(Libr123Msj.sport, 2)
                commit()
            }
            findNavController().navigate(R.id.mmain)
        }
        return q.root
    }
}