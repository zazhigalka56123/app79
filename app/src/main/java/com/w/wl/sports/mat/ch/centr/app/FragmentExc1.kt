package com.w.wl.sports.mat.ch.centr.app

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat.getColor
import androidx.core.content.edit
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import com.w.wl.sports.mat.ch.centr.app.R
import com.w.wl.sports.mat.ch.centr.app.databinding.Frag1Binding
import java.util.Calendar


class FragmentExc1: Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val fsdf = Frag1Binding.inflate(inflater, container, false)


        fsdf.bdsfds.setOnClickListener {
            try {
                val x = gusiLebedi(
                    fsdf.awesomebedroom.text.toString().toInt(),
                    fsdf.relxxqwe.text.toString().toInt(),
                    fsdf.godZombi.text.toString().toInt()
                )
                when (x) {
                    1 -> {
                        val sp = requireActivity().getSharedPreferences(
                            Libr123Msj.SPPP,
                            Context.MODE_PRIVATE
                        )
                        sp.edit {
                            putBoolean(Libr123Msj.pervyRaz, false)
                            commit()
                        }

                        findNavController().navigate(R.id.ewq)
                    }
                    2 -> {
                        val decorView = (requireActivity() as Activity).window.decorView
                        val view = decorView.findViewById(android.R.id.content) ?: decorView.rootView
                        Snackbar
                            .make(view, getString(R.string.n18dfsdfds), Snackbar.LENGTH_LONG)
                            .setBackgroundTint(getColor(requireContext(), R.color.dfsdfds))
                            .show()
                    }
                    else -> {
                        val decorView = (requireActivity() as Activity).window.decorView
                        val view = decorView.findViewById(android.R.id.content) ?: decorView.rootView
                        Snackbar
                            .make(view, getString(R.string.crwfcwefew), Snackbar.LENGTH_LONG)

                            .setBackgroundTint(getColor(requireContext(), R.color.dfsdfds))
                            .show()
                    }
                }
            }catch (e: Exception){
                val decorView = (requireActivity() as Activity).window.decorView
                val view = decorView.findViewById(android.R.id.content) ?: decorView.rootView
                Snackbar
                    .make(view, getString(R.string.werwerwe), Snackbar.LENGTH_LONG)
                    .setBackgroundTint(getColor(requireContext(), R.color.dfsdfds))
                    .show()
            }



        }

        return fsdf.root
    }

    private fun gusiLebedi(d: Int, m1: Int, y: Int): Int {
        val m = m1 - 1;
        val fsfdsf = Calendar.getInstance()

        fsfdsf.set(Calendar.DAY_OF_MONTH, d)
        fsfdsf.set(Calendar.MONTH, m)
        fsfdsf.set(Calendar.YEAR, y)

        return if(d in 1..31 && 0 <= m && m <= 11 && y <= 2023) {
            val gdfgfdpol = Calendar.getInstance()
            val hfdhd = (gdfgfdpol.timeInMillis - fsfdsf.timeInMillis)

            val gdfgfdpol2342 = Calendar.getInstance()
            gdfgfdpol2342.set(Calendar.DAY_OF_MONTH, 0)
            gdfgfdpol2342.set(Calendar.MONTH, 0)
            gdfgfdpol2342.set(Calendar.YEAR, 0)
            gdfgfdpol.timeInMillis = hfdhd
            gdfgfdpol2342.timeInMillis += hfdhd

            if (gdfgfdpol2342.get(Calendar.YEAR) >= 18) 1 else 2
        }else{
            0
        }
    }
}